# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 23:47:28 2017

@author: Feedlyy
"""




#app = Flask(__name__)
#@app.route("/")
#def get_news():
#    return "no news is good news"

#if __name__ == '__main__':
#    app.run(port=5001, debug=True)
    


#app2 = Flask(__name__)
import re
import feedparser
from flask import Flask
from flask import request
from flask import render_template

app = Flask(__name__)

RSS_FEEDS = {'empty': 'kosong', 
            'terkini': 'http://www.antaranews.com/rss/terkini',
            'nasional': 'http://www.antaranews.com/rss/nasional',
            'dunia': 'http://www.antaranews.com/rss/dunia',
            'ekonomi': 'http://www.antaranews.com/rss/ekonomi',
            'olahraga': 'http://www.antaranews.com/rss/olahraga',
            'hiburan': 'http://www.antaranews.com/rss/hiburan',
            'tekno': 'http://www.antaranews.com/rss/tekno',
            'warta': 'http://www.antaranews.com/rss/warta-bumi',
            'otomotif' : 'https://otomotif.antaranews.com/rss',
            'foto': 'http://www.antaranews.com/rss/photo.xml',
            'video': 'http://www.antaranews.com/rss/video.xml'}


@app.route("/", methods=['GET', 'POST'])
#@app.route("/<publication>")

def get_news():
    query = request.form.get("publication")
    if not query or query.lower() not in RSS_FEEDS:
        publication = "empty"
    else:
        publication = query.lower()
    feed = feedparser.parse(RSS_FEEDS[publication])
    return render_template("indexcuy.html", articles = feed['entries'])
    
if __name__ == "__main__":
    app.run(port=5010, debug=True)